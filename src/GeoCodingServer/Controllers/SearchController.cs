﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Controllers
{   
    [Route("search")]
    public class SearchController: ControllerBase
    {
        private readonly IConfiguration _configuration;
         
        public SearchController(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration)); ;
        }

        /// <summary>
        /// Hàm tìm kiếm dữ liệu từ server Nominatim 
        /// </summary>
        /// <param name="q"></param>
        /// <param name="street"></param>
        /// <param name="city"></param>
        /// <param name="county"></param>
        /// <param name="state"></param>
        /// <param name="country"></param>
        /// <param name="postalcode"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Search([FromQuery] string q, string street, string city, string county, string state, string country, string postalcode)
        {   
            string baseUrl = _configuration.GetSection("NominatimUrl").Value;

            var queryString = HttpContext.Request.QueryString;

            var url = baseUrl + "search" + queryString + "&format=json&addressdetails=1&limit=50&extratags=1";

            HttpClient http = new HttpClient();

            var data = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<object>(data);

            return Ok(result);
        }
    }
}
