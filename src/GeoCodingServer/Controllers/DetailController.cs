﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SI.GIS.BuildingServer.Core.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Controllers
{
    [Route("detail")]
    public class DetailController: ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IMediaRepository _mediaRepository;

        public DetailController(IConfiguration configuration, IMediaRepository mediaRepository)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _mediaRepository = mediaRepository;
        }

        /// <summary>
        /// GetDetails
        /// </summary>
        /// <param name="osmtype"></param>
        /// <param name="osmid"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetDetail([FromQuery] string osmtype, string osmid, string format, string pretty,
                                                   string addressdetails, string keywords, string hierarchy, string group_hierarchy, string polygon_geojson)
        {
            string baseUrl = _configuration.GetSection("NominatimUrl").Value;

            var queryString = HttpContext.Request.QueryString;

            var url = baseUrl + "details" + queryString;

            HttpClient http = new HttpClient();

            var data = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<dynamic>(data);

            var osm_id = (long)result.osm_id;

            var listMedia = _mediaRepository.TableNoTracking.Where(a => a.OsmId == osm_id).ToList();

            return Ok(new
            {
                place = result,
                media = listMedia
            });
        }
    }
}

 

