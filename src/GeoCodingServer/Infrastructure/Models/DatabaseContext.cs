﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SI.GIS.BuildingServer.Infrastructure.Models
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Media> Media { get; set; }
        public virtual DbSet<Place> Place { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Host=10.159.21.212;Port=5432;Database=locationEx;Username=postgres;Password=admin@123", x => x.UseNetTopologySuite());
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("hstore")
                .HasPostgresExtension("postgis");

            modelBuilder.Entity<Media>(entity =>
            {
                entity.ToTable("media");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Link)
                    .HasColumnName("link")
                    .HasColumnType("character varying");

                entity.Property(e => e.OsmId).HasColumnName("osm_id");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Place>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("place");

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.AdminLevel).HasColumnName("admin_level");

                entity.Property(e => e.Class)
                    .IsRequired()
                    .HasColumnName("class");

                entity.Property(e => e.Extratags).HasColumnName("extratags");

                entity.Property(e => e.Geometry)
                    .IsRequired()
                    .HasColumnName("geometry")
                    .HasColumnType("geometry(Geometry,4326)");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.OsmId).HasColumnName("osm_id");

                entity.Property(e => e.OsmType).HasColumnName("osm_type");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
