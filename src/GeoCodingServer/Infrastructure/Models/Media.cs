﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Models
{
    public partial class Media
    {
        public long Id { get; set; }
        public long OsmId { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
    }
}
