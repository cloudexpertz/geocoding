﻿using GIS.Persistence.PostgreSQL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;
using SI.GIS.BuildingServer.Infrastructure.Models;

using System;
using System.Reflection;

namespace SI.GIS.GeoCodingServer.Configurations
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddConnectionString(this IServiceCollection services, IConfiguration configuration)
        {
            // get default connection
            var strDDConnect = configuration.GetConnectionString("DefaultConnection");

            // add dbContext Postgresql database
            services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseNpgsql(strDDConnect, o => o.UseNetTopologySuite());
            });

            services.AddUnitOfWork<DatabaseContext>();
            // connection retry settings
            Action<NpgsqlDbContextOptionsBuilder> npgsqlOptionsAction = (o) =>
            {
                o.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                o.EnableRetryOnFailure(maxRetryCount: 5, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
            };

            return services;
        }
    }
}
