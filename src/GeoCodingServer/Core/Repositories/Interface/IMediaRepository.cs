﻿using GIS.Persistence.PostgreSQL;
using SI.GIS.BuildingServer.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Core.Repositories.Interface
{
    public interface IMediaRepository: IEfRepository<Media>
    {
    }
}
